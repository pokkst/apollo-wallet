package xyz.duudl3.apollo.tx;

import xyz.duudl3.apollo.Main;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;

public class BroadcastHelper {
    private boolean useTor;
    private boolean mainNet;
    private int responseCode = 0;

    public BroadcastHelper(boolean mainNet, boolean useTor)
    {
        this.useTor = useTor;
        this.mainNet = mainNet;
    }

    public void broadcast(final String txHex)
    {
        //Making a new thread is needed, as networking isn't allowed on the main thread, according to Android. :/
        new Thread(){
            @Override
            public void run()
            {
                String proxyString = "127.0.0.1:9050";
                String proxyAddress[] = proxyString.split(":");
                Proxy proxy = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(proxyAddress[0], Integer.parseInt(proxyAddress[1])));

                /**
                 * Thanks to the mule.tools project PonyDirect and Samourai Wallet itself for most of this code, because I was too lazy to write it myself.
                 */
                String requestUrl = null;
                if(mainNet)
                    requestUrl = "https://api.samouraiwallet.com/v2/pushtx/";
                else
                    requestUrl = "https://api.samouraiwallet.com/test/v2/pushtx/";

                URL url = null;
                try {
                    url = new URL(requestUrl);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                String contentType = null;
                String urlParameters = "tx=" + txHex;
                HttpURLConnection connection = null;

                try {
                    if(useTor)
                        connection = (HttpURLConnection)url.openConnection(proxy);
                    else
                        connection = (HttpURLConnection)url.openConnection();

                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setInstanceFollowRedirects(false);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestProperty("charset", "utf-8");
                    connection.setRequestProperty("Accept", "application/json");
                    connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));

                    //This is the user-agent of Tor Browser 8.0 on Windows, I tried to match this connection request to Tor as much as possible for maximum anonymity.
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0");

                    connection.setUseCaches (false);

                    connection.setConnectTimeout(60000);
                    connection.setReadTimeout(60000);

                    connection.connect();

                    DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                    wr.writeBytes(urlParameters);
                    wr.flush();
                    wr.close();

                    connection.setInstanceFollowRedirects(false);

                    if (connection.getResponseCode() == 200) {
                        System.out.println("Successfully broadcast transaction!");
                        javafx.application.Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                Main.instance.controller.toFee.clear();
                                Main.instance.controller.toAddress.clear();
                                Main.instance.controller.toAmount.clear();
                                Main.instance.controller.sendBtn.setDisable(false);
                                Main.instance.controller.toAmount.setDisable(false);
                                Main.instance.controller.toAddress.setDisable(false);
                                Main.instance.controller.toFee.setDisable(false);
                            }
                        });
                        responseCode = 200;
                    }
                    else
                    {
                        System.out.println("Failed to broadcast. Response code: " + connection.getResponseCode());
                        responseCode = connection.getResponseCode();
                    }

                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    connection.disconnect();
                }
            }
        }.start();
    }

    public int getResponseCode()
    {
        return responseCode;
    }
}
