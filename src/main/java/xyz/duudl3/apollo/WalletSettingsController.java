/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.duudl3.apollo;

import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.bitcoinj.wallet.DeterministicSeed;
import com.google.common.base.Splitter;
import com.google.common.util.concurrent.Service;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import xyz.duudl3.apollo.utils.GuiUtils;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class WalletSettingsController {

    public VBox innerWindow;
    public StackPane window;
    public HBox innerWindow2;
    @FXML TextArea walletRecoverySeed;
    @FXML TextArea wordsArea;
    @FXML Button restoreButton;

    public Main.OverlayUI overlayUI;


    public void initialize()
    {
        DeterministicSeed seed = Main.walletAppKit.wallet().getKeyChainSeed();

        final List<String> mnemonicCode = seed.getMnemonicCode();
        checkNotNull(mnemonicCode);    // Already checked for encryption.
        StringBuilder origWords = new StringBuilder();

        for (int x = 0; x < mnemonicCode.size(); x++) {
            origWords.append(mnemonicCode.get(x)).append(" ");
        }
        walletRecoverySeed.setText(origWords.toString());
    }

    public void closeClicked(ActionEvent event)
    {
        overlayUI.done();
    }

    public void restoreClicked(ActionEvent event) {
        if (Main.walletAppKit.wallet().getBalance().value > 0)
        {
            GuiUtils.informationalAlert("Vortex Notification",
                    "You must empty this wallet out before attempting to restore an older one, as mixing wallets " +
                            "together can lead to invalidated backups.");
            return;
        }

        overlayUI.done();
        Main.instance.controller.restoreFromSeedAnimation();

        DeterministicSeed seed = new DeterministicSeed(Splitter.on(' ').splitToList(wordsArea.getText()), null, "", 0);
        Main.walletAppKit.addListener(new Service.Listener()
        {
            @Override
            public void terminated(Service.State from)
            {
                Main.instance.setupWalletKit(seed);
                Main.walletAppKit.startAsync();
            }
        }, Platform::runLater
        );

        Main.walletAppKit.stopAsync();
    }

    public void openDonation(ActionEvent actionEvent) {
        try
        {
                Desktop.getDesktop().browse(new URI("https://duudl3.xyz/donate.html"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
