/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.duudl3.apollo.utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.utils.MonetaryFormat;
import org.bitcoinj.wallet.Wallet;
import org.bitcoinj.wallet.listeners.WalletChangeEventListener;
import org.bitcoinj.core.*;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import xyz.duudl3.apollo.Main;
import xyz.duudl3.apollo.MainController;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BitcoinUIModel {
    private SimpleObjectProperty<Address> address = new SimpleObjectProperty<>();
    private SimpleObjectProperty<Coin> balance = new SimpleObjectProperty<>(Coin.ZERO);
    private SimpleDoubleProperty syncProgress = new SimpleDoubleProperty(-1);
    private ProgressBarUpdater syncProgressUpdater = new ProgressBarUpdater();
    public static ObservableList<Transaction> transactions = FXCollections.observableArrayList();

    public BitcoinUIModel() {
    }

    public BitcoinUIModel(Wallet wallet) {
        setWallet(wallet);
    }

    public final void setWallet(Wallet wallet) {
        wallet.addChangeEventListener(Platform::runLater, new WalletChangeEventListener() {
            @Override
            public void onWalletChanged(Wallet wallet) {
                update(wallet);
            }
        });
        update(wallet);
    }

    private void update(Wallet wallet) {
        balance.set(wallet.getBalance());
        address.set(wallet.currentReceiveAddress());
        transactions.setAll(wallet.getTransactionsByTime());

        Main.instance.controller.txGrid.getChildren().clear();
        for(int x = 0; x < transactions.size(); x++)
        {
            int finalX = x;

            //  Label txLabel = new Label(transactions.get(x).getHash().toString());
            Image img = null;
            int confirmations = transactions.get(x).getConfidence().getDepthInBlocks();

            if(confirmations == 0)
                img = new Image(Main.class.getResourceAsStream("0.png"));
            else if(confirmations == 1)
                img = new Image(Main.class.getResourceAsStream("1.png"));
            else if(confirmations == 2)
                img = new Image(Main.class.getResourceAsStream("2.png"));
            else if(confirmations == 3)
                img = new Image(Main.class.getResourceAsStream("3.png"));
            else if(confirmations == 4)
                img = new Image(Main.class.getResourceAsStream("4.png"));
            else if(confirmations == 5)
                img = new Image(Main.class.getResourceAsStream("5.png"));
            else if(confirmations >= 6)
                img = new Image(Main.class.getResourceAsStream("6.png"));

            ImageView txConfImg = new ImageView(img);
            txConfImg.setFitWidth(16);
            txConfImg.setFitHeight(16);

            Label txConfirmations = new Label(transactions.get(x).getConfidence().getDepthInBlocks() + "");
            SimpleDateFormat dateFormatted = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Label txDate = new Label(dateFormatted.format(transactions.get(x).getUpdateTime()) + "");
            Label txHash = new Label(transactions.get(x).getHashAsString());
            Coin value = transactions.get(x).getValue(Main.walletAppKit.wallet());
            String amt = MonetaryFormat.BTC.format(value).toString().replace("BTC ", "");
            Label txAmt = new Label(amt);

            if(value.isNegative())
                txAmt.setTextFill(Color.web("#f00"));
            else
                txAmt.setTextFill(Color.web("#080"));

            txConfirmations.setOnMouseClicked(event -> printHash(txHash.getText(), finalX));
            txDate.setOnMouseClicked(event -> printHash(txHash.getText(), finalX));
            txHash.setOnMouseClicked(event -> printHash(txHash.getText(), finalX));
            txAmt.setOnMouseClicked(event -> printHash(txHash.getText(), finalX));
            Main.instance.controller.txGrid.add(txConfImg, 0, x, 1, 1);
            Main.instance.controller.txGrid.add(txDate, 1, x, 1, 1);
            Main.instance.controller.txGrid.add(txHash, 2, x, 1, 1);
            Main.instance.controller.txGrid.add(txAmt, 3, x, 1, 1);
        }
    }

    private void printHash(String txHash, int txIndex) {
        MainController.transactionSelected = txIndex;
        Main.instance.overlayUI("transaction.fxml");
        System.out.println(txHash);
    }

    private class ProgressBarUpdater extends DownloadProgressTracker {
        @Override
        protected void progress(double pct, int blocksLeft, Date date) {
            super.progress(pct, blocksLeft, date);
            Platform.runLater(() -> syncProgress.set(pct / 100.0));
        }

        @Override
        protected void doneDownload() {
            super.doneDownload();
            Platform.runLater(() -> syncProgress.set(1.0));
        }
    }

    public DownloadProgressTracker getDownloadProgressTracker() { return syncProgressUpdater; }

    public ReadOnlyDoubleProperty syncProgressProperty() { return syncProgress; }

    public ReadOnlyObjectProperty<Address> addressProperty() {
        return address;
    }

    public ReadOnlyObjectProperty<Coin> balanceProperty() {
        return balance;
    }

    public ObservableList<Transaction> getTransactions() {
        return transactions;
    }
}
