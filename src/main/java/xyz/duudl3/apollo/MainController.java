/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.duudl3.apollo;

import com.google.common.base.Splitter;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.Service;
import com.lambdaworks.jni.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import org.bitcoinj.core.Address;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.core.Coin;
import org.bitcoinj.crypto.MnemonicCode;
import org.bitcoinj.crypto.MnemonicException;
import org.bitcoinj.utils.MonetaryFormat;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.scene.layout.HBox;
import javafx.util.Duration;
import org.bitcoinj.utils.Threading;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.SendRequest;
import org.bitcoinj.wallet.Wallet;
import org.fxmisc.easybind.EasyBind;
import org.spongycastle.util.encoders.Hex;
import xyz.duudl3.apollo.controls.NotificationBarPane;
import xyz.duudl3.apollo.crypto.AES;
import xyz.duudl3.apollo.tx.BroadcastHelper;
import xyz.duudl3.apollo.utils.BitcoinUIModel;
import xyz.duudl3.apollo.utils.SettingsSaveManager;
import xyz.duudl3.apollo.utils.easing.EasingMode;
import xyz.duudl3.apollo.utils.easing.ElasticInterpolator;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static xyz.duudl3.apollo.utils.GuiUtils.checkGuiThread;
import static xyz.duudl3.apollo.utils.GuiUtils.crashAlert;
import static xyz.duudl3.apollo.utils.GuiUtils.informationalAlert;

/**
 * Gets created auto-magically by FXMLLoader via reflection. The widget fields are set to the GUI controls they're named
 * after. This class handles all the updates and event handling for the main UI.
 */
public class MainController {
    public HBox controlsBox;
    public Label balance;
    public Label connectionStatus;
    public static int transactionSelected;

    public static BitcoinUIModel model = new BitcoinUIModel();
    public Label txTitle;
    public AnchorPane window;
    public HBox balanceLabel;
    public GridPane txGrid;
    public TextField newReceiveAddress;
    public Tab receiveTab;
    public Tab sendTab;
    public ScrollPane historyBox;
    public TextField toFee;
    public TextField toAmount;
    public TextField toAddress;
    public Button sendBtn;
    public Button clearBtn;
    public TextField yourSeed;
    public TextField newSeed;
    public Button recoverBtn;
    public Pane firstStartPane;
    public Pane restorePane;
    public TabPane tabPane;
    public Pane newWalletPane;
    public Pane loadWalletPane;

    public PasswordField passwordField;
    public PasswordField passwordFieldRestore;
    public PasswordField loadPassword;
    private DeterministicSeed newWalletSeed;
    private List<String> mnemonicCode;

    // Called by FXMLLoader.
    public void initialize() {

    }

    public void onBitcoinSetup()
    {
        if(Main.walletAppKit != null) {
            model.setWallet(Main.walletAppKit.wallet());

            balance.textProperty().bind(EasyBind.map(model.balanceProperty(), coin -> MonetaryFormat.BTC.noCode().format(coin).toString()));

            //disable send  button when balance is 0.00000000, this isnt really needed, but i like it.
            sendTab.disableProperty().bind(model.balanceProperty().isEqualTo(Coin.ZERO));

            model.syncProgressProperty().addListener(x -> {
                if (model.syncProgressProperty().get() >= 1.0) {
                    connectionStatus.setText("Connected");
                    System.out.println("Wallet is synced");
                    readyToGoAnimation();
                } else {
                    connectionStatus.setText("Syncing... " + Main.walletAppKit.wallet().getLastBlockSeenHeight() + "/" + Main.walletAppKit.peerGroup().getDownloadPeer().getBestHeight());
                    System.out.println("Wallet is NOT synced");
                }
            });

            txGrid.getStyleClass().add("tx_grid_pane");

            receiveTab.setOnSelectionChanged(t -> {
                if (receiveTab.isSelected()) {
                    new Thread(() -> {
                        String address = Main.walletAppKit.wallet().freshReceiveAddress().toString();
                        javafx.application.Platform.runLater(() -> updateAddress(address));
                    }).start();
                }
            });

            newSeed.setDisable(false);
            recoverBtn.setDisable(false);
            newSeed.clear();
        }
    }

    private void updateAddress(String address) {
        newReceiveAddress.setText(address);
    }

    public void sendBitcoin(ActionEvent actionEvent) {
        Coin amount = Coin.parseCoin(toAmount.getText());

        if(amount.getValue() > 0.0 && Integer.parseInt(toFee.getText()) >= 1)
        {
            try
            {
                Address destination = Address.fromString(Main.params, toAddress.getText());
                SendRequest req = null;

                if (amount.equals(Main.walletAppKit.wallet().getBalance()))
                {
                    req = SendRequest.emptyWallet(destination);
                }
                else
                {
                    req = SendRequest.to(destination, amount);
                }

                req.ensureMinRequiredFee = false;
                //converting because the UI uses sats/byte, so we need to convert that to fee per kb here
                req.feePerKb = Coin.valueOf(Long.parseLong(toFee.getText()) * 1000L);
                Transaction tx = Main.walletAppKit.wallet().sendCoinsOffline(req);
                byte[] txHexBytes = Hex.encode(tx.bitcoinSerialize());
                String txHex = new String(txHexBytes, "UTF-8");
                sendBtn.setDisable(true);
                toAmount.setDisable(true);
                toAddress.setDisable(true);
                toFee.setDisable(true);
                BroadcastHelper helper = new BroadcastHelper(!SettingsSaveManager.useTestnet, true);
                helper.broadcast(txHex);
            }
            catch (InsufficientMoneyException e)
            {
                informationalAlert("Apollo Notification",
                        "You do not have enough Bitcoin for this transaction.");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    public void clearSendFields(ActionEvent actionEvent) {
        toFee.clear();
        toAddress.clear();
        toAmount.clear();
    }

    public void createNewWallet(ActionEvent actionEvent) {
        //DEFAULT_SEED_ENTROPY_BITS * 2 generates a 24 word seed.
        byte[] entropy = getEntropy(DeterministicSeed.DEFAULT_SEED_ENTROPY_BITS * 2);
        List<String> mnemonic = null;
        try {
            mnemonic = MnemonicCode.INSTANCE.toMnemonic(entropy);
        } catch (MnemonicException.MnemonicLengthException e) {
            e.printStackTrace();
        }

        setScreen("createdWalletSeed");
        mnemonicCode = mnemonic;
        StringBuilder recoverySeed = new StringBuilder();

        for (int x = 0; x < mnemonicCode.size(); x++) {
            recoverySeed.append(mnemonicCode.get(x)).append(x == mnemonicCode.size() - 1 ? "" : " ");
        }

        String seedStr = recoverySeed.toString();
        yourSeed.setText(seedStr);

        connectionStatus.setVisible(false);
        Main.instance.settings.setData("firstStart", "false");
    }

    private static byte[] getEntropy(int bits) {
        return getEntropy(new SecureRandom(), bits);
    }

    private static byte[] getEntropy(SecureRandom random, int bits) {
        checkArgument(bits <= DeterministicSeed.MAX_SEED_ENTROPY_BITS, "requested entropy size too large");

        byte[] seed = new byte[bits / 8];
        random.nextBytes(seed);
        return seed;
    }

    public void recoverWallet(ActionEvent actionEvent) {
        if(!passwordFieldRestore.getText().equals("")) {
            DeterministicSeed seed = new DeterministicSeed(Splitter.on(' ').splitToList(newSeed.getText()), null, passwordFieldRestore.getText(), 1540098000);
            Main.instance.setupWalletKit(seed);
            setScreen("startRestore");
            List<String> mnemonicCode = seed.getMnemonicCode();
            StringBuilder recoverySeed = new StringBuilder();

            for (int x = 0; x < mnemonicCode.size(); x++) {
                recoverySeed.append(mnemonicCode.get(x)).append(x == mnemonicCode.size() - 1 ? "" : " ");
            }

            String seedStr = recoverySeed.toString();
            Main.instance.settings.setData("encryptedSeed", AES.encrypt(seedStr, passwordFieldRestore.getText()));

            Main.instance.settings.setData("firstStart", "false");
        }
    }

    public void restoreFromSeedAnimation() {
        // Buttons slide out ...
        TranslateTransition leave = new TranslateTransition(Duration.millis(1200), controlsBox);
        leave.setByY(80.0);
        leave.play();
    }

    public void readyToGoAnimation() {
        // Buttons slide in and clickable address appears simultaneously.
        TranslateTransition arrive = new TranslateTransition(Duration.millis(1200), controlsBox);
        arrive.setInterpolator(new ElasticInterpolator(EasingMode.EASE_OUT, 1, 2));
        arrive.setToY(0.0);
    }

    public DownloadProgressTracker progressBarUpdater() {
        return model.getDownloadProgressTracker();
    }

    public void displayRestorePane(ActionEvent actionEvent) {
        setScreen("restore");
    }

    public void ackSeed(ActionEvent actionEvent) {
        if(passwordField.getText() != null && !passwordField.getText().isEmpty()) {
            StringBuilder recoverySeed = new StringBuilder();

            for (int x = 0; x < mnemonicCode.size(); x++) {
                recoverySeed.append(mnemonicCode.get(x)).append(x == mnemonicCode.size() - 1 ? "" : " ");
            }

            newWalletSeed = new DeterministicSeed(Splitter.on(' ').splitToList(recoverySeed.toString()), null, passwordField.getText(), 1540098000);
            Main.instance.setupWalletKit(newWalletSeed);
            String seedStr = recoverySeed.toString();

            Main.instance.settings.setData("encryptedSeed", AES.encrypt(seedStr, passwordField.getText()));
            setScreen("ackSeed");
        }
    }

    public void loadWallet(ActionEvent actionEvent) {
        String encryptedSeed = Main.instance.settings.getData("encryptedSeed");
        String decryptedSeed = AES.decrypt(encryptedSeed, loadPassword.getText());

        if(!decryptedSeed.equals("failedDecrypt")) {
            DeterministicSeed seed = new DeterministicSeed(Splitter.on(' ').splitToList(decryptedSeed), null, loadPassword.getText(), 1540098000);
            Main.instance.setupWalletKit(seed);
            setScreen("loadWallet");
        }
    }

    public void setScreen(String scene)
    {
        if(scene.equals("firstStartPane"))
        {
            newWalletPane.setVisible(false);
            firstStartPane.setVisible(true);
            restorePane.setVisible(false);
            tabPane.setVisible(false);
            connectionStatus.setVisible(false);
            balanceLabel.setVisible(false);
            loadWalletPane.setVisible(false);
        }
        else if(scene.equals("ackSeed"))
        {
            newWalletPane.setVisible(false);
            firstStartPane.setVisible(false);
            restorePane.setVisible(false);
            tabPane.setVisible(true);
            connectionStatus.setVisible(true);
            balanceLabel.setVisible(true);
            loadWalletPane.setVisible(false);
        }
        else if(scene.equals("restore"))
        {
            newWalletPane.setVisible(false);
            firstStartPane.setVisible(false);
            restorePane.setVisible(true);
            tabPane.setVisible(false);
            connectionStatus.setVisible(false);
            balanceLabel.setVisible(false);
            loadWalletPane.setVisible(false);
        }
        else if(scene.equals("startRestore"))
        {
            newWalletPane.setVisible(false);
            firstStartPane.setVisible(false);
            restorePane.setVisible(false);
            tabPane.setVisible(true);
            connectionStatus.setVisible(true);
            balanceLabel.setVisible(true);
            loadWalletPane.setVisible(false);
        }
        else if(scene.equals("loadWallet"))
        {
            loadWalletPane.setVisible(false);
            newWalletPane.setVisible(false);
            firstStartPane.setVisible(false);
            restorePane.setVisible(false);
            tabPane.setVisible(true);
            connectionStatus.setVisible(true);
            balanceLabel.setVisible(true);
        }
        else if(scene.equals("createdWalletSeed"))
        {
            newWalletPane.setVisible(true);
            firstStartPane.setVisible(false);
            restorePane.setVisible(false);
            tabPane.setVisible(false);
            connectionStatus.setVisible(false);
            balanceLabel.setVisible(false);
            loadWalletPane.setVisible(false);
        }
    }
}
