/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.duudl3.apollo;

import com.google.common.base.Splitter;
import com.google.common.util.concurrent.*;
import com.google.protobuf.ByteString;
import javafx.scene.image.Image;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.crypto.KeyCrypterScrypt;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.params.*;
import org.bitcoinj.utils.BriefLogFormatter;
import org.bitcoinj.utils.Threading;
import org.bitcoinj.wallet.DeterministicSeed;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.bitcoinj.wallet.Protos;
import xyz.duudl3.apollo.controls.NotificationBarPane;
import xyz.duudl3.apollo.utils.GuiUtils;
import xyz.duudl3.apollo.utils.SettingsSaveManager;
import xyz.duudl3.apollo.utils.TextFieldValidator;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.nio.channels.FileLock;

import static xyz.duudl3.apollo.utils.GuiUtils.*;

public class Main extends Application {
    public static NetworkParameters params = null;
    public static final String APP_NAME = "Apollo";
    public static String WALLET_FILE_NAME = "";
    public static WalletAppKit walletAppKit;
    public static Main instance;

    private StackPane uiStack;
    private Pane mainUI;
    public MainController controller;
    public NotificationBarPane notificationBar;
    public Stage mainWindow;
    public SettingsSaveManager settings;
    public static final Protos.ScryptParameters SCRYPT_PARAMETERS = Protos.ScryptParameters.newBuilder()
            .setP(6)
            .setR(8)
            .setN(32768)
            .setSalt(ByteString.copyFrom(KeyCrypterScrypt.randomSalt()))
            .build();

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage mainWindow) throws Exception {
        try {
            loadWindow(mainWindow);
        } catch (Throwable e) {
            GuiUtils.crashAlert(e);
            throw e;
        }
    }

    private void loadWindow(Stage mainWindow) throws IOException {
        this.mainWindow = mainWindow;
        instance = this;
        settings = new SettingsSaveManager(SettingsSaveManager.saveFile);
        settings.loadSettings();

        if(SettingsSaveManager.useTestnet)
        {
            params = TestNet3Params.get();
        }
        else
        {
            params = MainNetParams.get();
        }

        WALLET_FILE_NAME = APP_NAME.replaceAll("[^a-zA-Z0-9.-]", "_") + "-" + params.getPaymentProtocolId() + "net";

        File chainFile = new File(".", WALLET_FILE_NAME + ".spvchain");
        chainFile.delete();
        File walletFile = new File(".", WALLET_FILE_NAME + ".wallet");
        walletFile.delete();

        File folder = new File(".");
        File[] files = folder.listFiles();
        assert files != null;
        for (File file : files) {
            if (file.isFile() && file.getName().contains("wallet") && file.getName().contains(".tmp")) {
                file.delete();
            }
        }

        GuiUtils.handleCrashesOnThisThread();

        URL location = getClass().getResource("main.fxml");
        FXMLLoader loader = new FXMLLoader(location);
        mainUI = loader.load();
        controller = loader.getController();
        notificationBar = new NotificationBarPane(mainUI);
        mainWindow.setTitle(APP_NAME);
        uiStack = new StackPane();
        Scene scene = new Scene(uiStack);
        TextFieldValidator.configureScene(scene);
        scene.getStylesheets().add(getClass().getResource("wallet.css").toString());
        uiStack.getChildren().add(notificationBar);
        mainWindow.setScene(scene);
        mainWindow.setResizable(false);
        //saving this for later for when i can work out .fxml scaling
        //mainWindow.setMaxWidth(Screen.getPrimary().getBounds().getWidth() / 2);
        //mainWindow.setMaxHeight(Screen.getPrimary().getBounds().getHeight() / 2);
        mainWindow.setMaxWidth(800);
        mainWindow.setMaxHeight(451);

        //using an online image to update the icon on the fly without needing to push new builds, just because i want to.
        mainWindow.getIcons().add(new Image("https://duudl3.xyz/img/vortex_wallet_logo.png"));

        BriefLogFormatter.init();

        Threading.USER_THREAD = Platform::runLater;

        if(!SettingsSaveManager.isFirstStart)
        {
            if(settings.getData("encryptedSeed") == null)
            {
                settings.setData("firstStart", "true");
                controller.newWalletPane.setVisible(false);
                controller.firstStartPane.setVisible(true);
                controller.restorePane.setVisible(false);
                controller.tabPane.setVisible(false);
                controller.connectionStatus.setVisible(false);
                controller.balanceLabel.setVisible(false);
                controller.loadWalletPane.setVisible(false);
            }
            else {
                controller.loadWalletPane.setVisible(true);
                controller.newWalletPane.setVisible(false);
                controller.firstStartPane.setVisible(false);
                controller.restorePane.setVisible(false);
                controller.tabPane.setVisible(false);
                controller.connectionStatus.setVisible(false);
                controller.balanceLabel.setVisible(false);
                //setupWalletKit(null);
            }
        }
        else
        {
            settings.setData("firstStart", "true");
            controller.newWalletPane.setVisible(false);
            controller.firstStartPane.setVisible(true);
            controller.restorePane.setVisible(false);
            controller.tabPane.setVisible(false);
            controller.connectionStatus.setVisible(false);
            controller.balanceLabel.setVisible(false);
            controller.loadWalletPane.setVisible(false);
        }

        mainWindow.show();
    }

    public void setupWalletKit(DeterministicSeed seed) {
        // If seed is non-null it means we are restoring from backup.
        walletAppKit = new WalletAppKit(params, new File("."), WALLET_FILE_NAME)
        {
            @Override
            protected void onSetupCompleted() {
                walletAppKit.wallet().allowSpendingUnconfirmedTransactions();
                Platform.runLater(controller::onBitcoinSetup);
            }
        };

        if (seed != null) {
            walletAppKit.restoreWalletFromSeed(seed);
        }

        walletAppKit.setDownloadListener(controller.progressBarUpdater());
        walletAppKit.setBlockingStartup(false);
        walletAppKit.startAsync();
    }

    private Node stopClickPane = new Pane();

    public class OverlayUI<T>
    {
        public Node ui;
        public T controller;

        public OverlayUI(Node ui, T controller)
        {
            this.ui = ui;
            this.controller = controller;
        }

        public void show()
        {
            checkGuiThread();
            if (currentOverlay == null)
            {
                uiStack.getChildren().add(stopClickPane);
                uiStack.getChildren().add(ui);
                blurOut(mainUI);
                fadeIn(ui);
                zoomIn(ui);
            }
            else
            {
                explodeOut(currentOverlay.ui);
                fadeOutAndRemove(uiStack, currentOverlay.ui);
                uiStack.getChildren().add(ui);
                ui.setOpacity(0.0);
                fadeIn(ui, 100);
                zoomIn(ui, 100);
            }
            currentOverlay = this;
        }

        public void done() {
            checkGuiThread();
            if (ui == null) return;  // In the middle of being dismissed and got an extra click.
            explodeOut(ui);
            fadeOutAndRemove(uiStack, ui, stopClickPane);
            blurIn(mainUI);
            this.ui = null;
            this.controller = null;
            currentOverlay = null;
        }
    }

    private OverlayUI currentOverlay;

    //loads given .fxml file and displays it onto the Scene.
    public <T> OverlayUI<T> overlayUI(String name)
    {
        try
        {
            checkGuiThread();
            URL location = GuiUtils.getResource(name);
            FXMLLoader loader = new FXMLLoader(location);
            Pane ui = loader.load();
            T controller = loader.getController();
            OverlayUI<T> pair = new OverlayUI<T>(ui, controller);

            try
            {
                if (controller != null)
                    controller.getClass().getField("overlayUI").set(controller, pair);
            }
            catch (IllegalAccessException | NoSuchFieldException ignored)
            {
                //my program is fucking flawless so i wont need this shit tbh
                ignored.printStackTrace();
            }

            pair.show();
            return pair;
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void stop() throws IOException, InterruptedException {
        if(walletAppKit != null) {
            walletAppKit.stopAsync();
            walletAppKit.awaitTerminated();

            /**
             * If you close Apollo before the chain finishes downloading, it creates .tmp wallet files. We do not want this. So we find all of the files and delete them.
             */
            while(true) {
                File walletFile = new File(".", Main.WALLET_FILE_NAME + ".wallet");

                boolean walletFileDel = false;
                if(!walletAppKit.isChainFileLocked())
                {
                    walletFileDel = walletFile.delete();
                }

                if(walletFileDel) {
                    File folder = new File(".");
                    File[] files = folder.listFiles();
                    assert files != null;
                    for (File file : files) {
                        if (file.isFile() && file.getName().contains("wallet") && file.getName().contains(".tmp")) {
                            file.delete();
                        }
                    }

                    break;
                }
            }
        }

        System.exit(0);
    }
}
