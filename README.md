# Apollo Wallet

A security and privacy focused wallet. Your wallet is destroyed when closing the program down, and is restored when opening it up. Password protection is mandatory. Sending coins through Samourai's API through Tor is mandatory.

(Transferred over from my old GitLab account)